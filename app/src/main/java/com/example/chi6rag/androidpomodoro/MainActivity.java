package com.example.chi6rag.androidpomodoro;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView output;
    private BroadcastReceiver pomodoroInfoReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        output = (TextView) findViewById(R.id.timer);
        findViewById(R.id.toggle_timer_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pomodoroServiceIntent = new Intent(MainActivity.this, PomodoroService.class);
                startService(pomodoroServiceIntent);
            }
        });

        pomodoroInfoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                long elapsedTime = intent.getLongExtra("elapse-time", 0);
                boolean hasCompleted = intent.getBooleanExtra("status", false);
                output.setText(String.format(PomodoroService.MyTimerTask.TIMER_DISPLAY_CONFIGURATION, elapsedTime / 60, elapsedTime % 60));
                if (hasCompleted) {
                    Toast.makeText(getApplicationContext(), "Finished", Toast.LENGTH_SHORT).show();
                    Intent intentservice = new Intent(MainActivity.this, PomodoroService.class);
                    stopService(intentservice);
                }
            }
        };
        onNewIntent(getIntent());
    }

    private boolean isTimerDoneStatusTrue() {
        return getIntent().getBooleanExtra(PomodoroService.TIMER_DONE, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pomodoroInfoReceiver);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(isTimerDoneStatusTrue()) {
            Toast.makeText(this, "Timer Completed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(pomodoroInfoReceiver, new IntentFilter(PomodoroService.MyTimerTask.POMODORO_INTENT_FILTER));
    }

//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        listener.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        listener.onRestoreInstanceState(savedInstanceState);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        listener.onDestroy();
//    }
}
