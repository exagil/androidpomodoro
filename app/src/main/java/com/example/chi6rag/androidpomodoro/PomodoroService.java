package com.example.chi6rag.androidpomodoro;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by chi6rag on 12/9/15.
 */
public class PomodoroService extends Service {
    private static final String START_TIME_KEY = "start_time";
    private static final String POMORODO_PREFERENCES_KEY = "pomorodo_preferences";
    private static final int NOTIFICATION_ID = 1;
    public static final String TIMER_DONE = "timer_done";
    private boolean isRunning;
    private long startTime;
    private Timer timer;

    public void finish() {
        isRunning = false;
        timer.cancel();
    }

    private void startTimer() {
        timer = new Timer();
        TimerTask timerTask = new MyTimerTask(this, startTime);
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    public void onDestroy() {
        if (timer != null) timer = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isRunning) return START_STICKY;

        isRunning = true;
        SharedPreferences pomodoroPreferences = getSharedPreferences(POMORODO_PREFERENCES_KEY, Context.MODE_PRIVATE);
        if (pomodoroPreferences.contains(START_TIME_KEY)) {
            startTime = fetchStartTimeFromSharedPreferences(pomodoroPreferences);
        } else {
            startTime = new Date().getTime();
            putStartTimeInSharedPreferences(pomodoroPreferences);
        }
        startTimer();


        startForegroundNotificationWithText("Started");
        return START_STICKY;
    }

    private void startForegroundNotificationWithText(String text) {
        NotificationCompat.Builder notificationCompatBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.mipmap.ic_stat_pomodoro)
                .setContentTitle("Pomodoro")
                .setContentText(text);

        Intent pomodoroNotificationIntent = new Intent(getApplicationContext(), MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingPomodoroNotificationIntent = PendingIntent.getActivity(getApplicationContext(), NOTIFICATION_ID, pomodoroNotificationIntent, 0);

        notificationCompatBuilder.setContentIntent(pendingPomodoroNotificationIntent);
        startForeground(NOTIFICATION_ID, notificationCompatBuilder.build());
    }

    private long fetchStartTimeFromSharedPreferences(SharedPreferences pomodoroPreferences) {
        return pomodoroPreferences.getLong(START_TIME_KEY, 0);
    }

    private void putStartTimeInSharedPreferences(SharedPreferences pomodoroPreferences) {
        SharedPreferences.Editor pomodoroPreferencesEditor = pomodoroPreferences.edit();
        pomodoroPreferencesEditor.putLong(START_TIME_KEY, startTime);
        pomodoroPreferencesEditor.commit();
    }

    private void removeStartTimeFromSharedPreferences() {
        SharedPreferences pomodoroPreferences = getSharedPreferences(POMORODO_PREFERENCES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor pomodoroPreferencesEditor = pomodoroPreferences.edit();
        pomodoroPreferencesEditor.remove(START_TIME_KEY);
        pomodoroPreferencesEditor.commit();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class MyTimerTask extends TimerTask {
        public static final String TIMER_DISPLAY_CONFIGURATION = "%02d:%02d";
        public static final String POMODORO_INTENT_FILTER = "pomodoro-information";
        public final Context mContext;
        public final long startTime;

        public MyTimerTask(Context context, long startTime) {
            mContext = context;
            this.startTime = startTime;
        }

        @Override
        public void run() {
            final long elapsedTime = (new Date().getTime() - startTime) / 1000;
            Intent intent = new Intent(POMODORO_INTENT_FILTER);
            intent.putExtra("elapse-time", elapsedTime);
            if (elapsedMinutes(elapsedTime) >= 1) {
                intent.putExtra("status", true);
            }
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            if (elapsedMinutes(elapsedTime) >= 1) {

                Intent timerDoneIntent = new Intent(getApplicationContext(), MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                timerDoneIntent.putExtra(TIMER_DONE, true);
                startActivity(timerDoneIntent);

                cancel();
                stopSelf();
                removeStartTimeFromSharedPreferences();
                stopForeground(true);
            }
            startForegroundNotificationWithText(fomattedNotification(elapsedTime));
        }

        private String fomattedNotification(long elapsedTime) {
            return String.format(PomodoroService.MyTimerTask.TIMER_DISPLAY_CONFIGURATION, elapsedTime / 60, elapsedTime % 60);
        }

        private long elapsedSeconds(long elapsedTime) {
            return elapsedTime % 60;
        }

        private long elapsedMinutes(long elapsedTime) {
            return elapsedTime / 60;
        }
    }
}
